#!/usr/bin/env python
import sys
from PIL import Image
import ephem
import datetime
import matplotlib.pyplot as plt
import numpy as np
import requests
import json
import shutil
from mutagen.oggvorbis import OggVorbis
import re
import os.path

plt.switch_backend('Agg')

print(sys.argv[1])

try:
    obs_id = int(sys.argv[1])
except:
    print('Please give a valid observation id')
    sys.exit()

print("Requesting observation " + sys.argv[1])
r = requests.get('https://network.satnogs.org/api/observations/' + sys.argv[1] + '/?format=json')
observation = json.loads(r.content.decode('utf8'))
print(observation)


print("Downloading the observation page for getting frequency and tle")
observation_link = "https://network.satnogs.org/observations/" + sys.argv[1]
obs_html = requests.get(observation_link).content
tle_regex = r"<pre>(1 .*)<br>(2 .*)</pre>"
tle_matches = re.search(tle_regex, obs_html.decode("utf-8"))
freq_regex = r"(\d*\.\d*) MHz"
freq_matches = re.findall(freq_regex, obs_html.decode("utf-8")).pop()

if not os.path.isfile(sys.argv[1] + '.ogg'):
    if observation['payload']:
        print("Downloading " + observation['payload'] + ' for getting the right duration')
        observation_ogg_data = requests.get(observation['payload'], stream=True).content
    else:
        print("Downloading " + observation['archive_url'] + ' for getting the right duration')
        observation_ogg_data = requests.get(observation['archive_url'], stream=True).content
    with open(sys.argv[1] + '.ogg', 'wb') as out_file:
        out_file.write(observation_ogg_data)

if not os.path.isfile(sys.argv[1] + '.png'):
    print("Downloading " + observation['waterfall'] + ' for the background image')
    img_data = requests.get(observation['waterfall']).content
    with open(sys.argv[1] + '.png', 'wb') as handler:
        handler.write(img_data)

# Set tle
tle0 = "Estimated TLE"
tle1 = tle_matches.group(1)
tle2 = tle_matches.group(2)
satellite1 = ephem.readtle(tle0, tle1, tle2)

observer = ephem.Observer()
observer.lat = str(observation['station_lat'])
observer.lon = str(observation['station_lng'])
observer.elevation = observation['station_alt']
freq0 = float(freq_matches)
tstart = observation['start'].replace('Z', '')
start = datetime.datetime.strptime(tstart, "%Y-%m-%dT%H:%M:%S")
ystart = datetime.datetime(start.year, 1, 1)
diff = start-ystart
#+1 day in timedelta as TLE show the day and a fraction of it so Jan 1st is 1 not 0.
obs_epoch_day = ((start - ystart).total_seconds() + datetime.timedelta(days=1).total_seconds()) / datetime.timedelta(days=1).total_seconds()
f = OggVorbis(sys.argv[1] + '.ogg')
nseconds = int(round(f.info.length))

tle_file = 'tle'
if len(sys.argv) > 2:
    tle_file = sys.argv[2]
with open(tle_file) as f:
    tle_lines = f.read().splitlines()
tles = []
for i in range(0, len(tle_lines), 3):
     tles.append(tle_lines[i:i + 3])

for tba_tle in tles:
    satellite2 = ephem.readtle(tba_tle[0], tba_tle[1], tba_tle[2])
    epoch_regex = r".{20}\s*(\d*\.\d*)"
    epoch_matches = re.search(epoch_regex, tba_tle[1])
    epoch_day = epoch_matches.group(1)
    obs_tle_epochs_diff = float(epoch_day) - obs_epoch_day
    bef_after = 'B'
    if obs_tle_epochs_diff > 0:
        bef_after = 'A'
    if abs(obs_tle_epochs_diff) > 2:
        continue
    print(obs_tle_epochs_diff)

    times = [start+datetime.timedelta(seconds=s) for s in range(0, nseconds)]

    v1 = []
    v2 = []
    for t in times:
        observer.date = t
        satellite1.compute(observer)
        satellite2.compute(observer)
        v1.append(satellite1.range_velocity)
        v2.append(satellite2.range_velocity)

    freq1 = freq0*(1.0-np.array(v1)/299792458.0)
    freq2 = freq0*(1.0-np.array(v2)/299792458.0)
    dfreq = (freq2-freq1)*1000.0

    t = np.arange(nseconds)

    plt.figure(figsize=(10 * 0.8,20))
    plt.plot(dfreq, t, color='red', linewidth=0.5)
    plt.ylabel("Time (seconds)")
    plt.xlabel("Frequency (kHz)")
    plt.xlim(-24.0, 24.0)
    plt.ylim(0, nseconds)
    plt.savefig(sys.argv[1] + ' ' + tba_tle[0].replace('/','_') + "_freq_diff.png", bbox_inches="tight", transparent=True)
    plt.close()
    background = Image.open(sys.argv[1] + ".png")
    overlay = Image.open(sys.argv[1] + ' ' + tba_tle[0].replace('/','_') + "_freq_diff.png")

    background = background.convert("RGBA")
    overlay = overlay.convert("RGBA")

    ov = Image.new('RGBA',background.size, (0, 0, 0, 0))
    ov.paste(overlay, overlay.getbbox())
    background.paste(ov, (0,0), ov)
    if not os.path.exists(sys.argv[1]):
        os.makedirs(sys.argv[1])
    background.save(sys.argv[1] + '/' + tstart + '_' + sys.argv[1] + '_' + tba_tle[0].replace('/','_') + '_' + str(abs(obs_tle_epochs_diff)) + '_' + bef_after + ".png","PNG")
    background.close()
    overlay.close()

